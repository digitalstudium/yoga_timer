import 'package:flutter/material.dart';
import 'package:yoga_timer/kriya_page.dart';
import 'package:yoga_timer/universal_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 2,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.all(8),
            color: Colors.teal[100],
            child: TextButton(
                child: const Center(child: Text("Крия йога")),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return KriyaPage();
                  }));
                })),
        Container(
            padding: const EdgeInsets.all(8),
            color: Colors.teal[100],
            child: TextButton(
                child: const Center(child: Text("Универсальный таймер")),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return UniversalPage();
                  }));
                })),
      ],
    );
  }
}
