import 'package:flutter/material.dart';
import 'package:yoga_timer/home_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Йога таймер',
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Йога таймер'),
            ),
            body: HomePage()));
  }
}
