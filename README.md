# yoga_timer

Содержит два вида таймера: для занятий крия йогой и универсальный интервальный таймер.
Скачать можно здесь: https://git.digitalstudium.com/digitalstudium/yoga_timer/releases/download/1.0.0/app-release.apk

## Компиляция
```
flutter build apk
```

В результате появится файл `build/app/outputs/apk/release/app-release.apk`, который можно скачать на 
Android-смартфон и установить.